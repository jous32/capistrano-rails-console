# frozen_string_literal: true

require 'sshkit/interactive'
require 'capistrano/rails/console/version'

load File.expand_path('console/tasks.cap', __dir__)
